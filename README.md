# Papers

Robotics:
1. Bio-Inspired Soft Robotics for Exploration of Unknown Environments.
2. Assistance Robotics for support disabilities conditions.

Neural Networks:

3. Expectation models for Multimodal Attention.

4. MS final project

Publications:

Human-robot interaction:

5.  Virtual or Physical? Social Robots Teaching a Fictional Language Through a Role-Playing Game Inspired by Game of Thrones
 https://www.researchgate.net/profile/Tayfun-Alpay/publication/337418392_Virtual_or_Physical_Social_Robots_Teaching_a_Fictional_Language_Through_a_Role-Playing_Game_Inspired_by_Game_of_Thrones/links/5ddba8cda6fdccdb44631f07/Virtual-or-Physical-Social-Robots-Teaching-a-Fictional-Language-Through-a-Role-Playing-Game-Inspired-by-Game-of-Thrones.pdf

Machine Learning:

6. (Abstract) Predicting condition ratings of bridges using Machine Learning models - Conference Statistical Week 2023 - TU Dortmund

Using DNNs (e.g. GRU) ad ML (e.g. KNN) methods for Bridges conditions predictions

*Book of abstracts will be available in close future

Data science:

7. (Abstract) Investigating synthetic pedestrian data generation to improve mobility patterns recognition of citizens for Smarter cities in the context of Smart cities.

Data science to evaluate quality of synthetic pedestrian data

https://www.researchgate.net/publication/362387982_Investigating_Synthetic_Pedestrian_Data_Generation_to_Improve_Mobility_Patterns_Recognition_of_Citizens_for_Smarter_Cities




